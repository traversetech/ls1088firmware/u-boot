// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright 2017 NXP
 */

#include <common.h>
#include <command.h>
#include <netdev.h>
#include <malloc.h>
#include <fsl_mdio.h>
#include <miiphy.h>
#include <phy.h>
#include <fm_eth.h>
#include <asm/io.h>
#include <exports.h>
#include <asm/arch/fsl_serdes.h>
#include <fsl-mc/fsl_mc.h>
#include <fsl-mc/ldpaa_wriop.h>

uint8_t phys_configured = 0;

int board_eth_init(bd_t *bis)
{
#if defined(CONFIG_FSL_MC_ENET)
	int i, interface;
	struct memac_mdio_info mdio_info;
	struct mii_dev *dev;
	struct ccsr_gur *gur = (void *)(CONFIG_SYS_FSL_GUTS_ADDR);
	struct memac_mdio_controller *reg;
	u32 srds_s1, cfg;

	cfg = in_le32(&gur->rcwsr[FSL_CHASSIS3_SRDS1_REGSR - 1]) &
				FSL_CHASSIS3_SRDS1_PRTCL_MASK;
	cfg >>= FSL_CHASSIS3_SRDS1_PRTCL_SHIFT;

	srds_s1 = serdes_get_number(FSL_SRDS_1, cfg);

	reg = (struct memac_mdio_controller *)CONFIG_SYS_FSL_WRIOP1_MDIO1;
	mdio_info.regs = reg;
	mdio_info.name = DEFAULT_WRIOP_MDIO1_NAME;

	/* Register the EMI 1 */
	fm_memac_mdio_init(bis, &mdio_info);
	printf("DEBUG: MDIO1 registered\n");

	reg = (struct memac_mdio_controller *)CONFIG_SYS_FSL_WRIOP1_MDIO2;
	mdio_info.regs = reg;
	mdio_info.name = DEFAULT_WRIOP_MDIO2_NAME;

	switch (srds_s1) {
	case 0x1D:
		/*
		 * XFI does not need a PHY to work, but to avoid U-boot use
		 * default PHY address which is zero to a MAC when it found
		 * a MAC has no PHY address, we give a PHY address to XFI
		 * MAC error.
		 */
		wriop_set_phy_address(WRIOP1_DPMAC1, 0, 0x0a);
		wriop_set_phy_address(WRIOP1_DPMAC3, 0, QSGMII1_PORT1_PHY_ADDR);
		wriop_set_phy_address(WRIOP1_DPMAC4, 0, QSGMII1_PORT2_PHY_ADDR);
		wriop_set_phy_address(WRIOP1_DPMAC5, 0, QSGMII1_PORT3_PHY_ADDR);
		wriop_set_phy_address(WRIOP1_DPMAC6, 0, QSGMII1_PORT4_PHY_ADDR);
		wriop_set_phy_address(WRIOP1_DPMAC7, 0, QSGMII2_PORT1_PHY_ADDR);
		wriop_set_phy_address(WRIOP1_DPMAC8, 0, QSGMII2_PORT2_PHY_ADDR);
		wriop_set_phy_address(WRIOP1_DPMAC9, 0, QSGMII2_PORT3_PHY_ADDR);
		wriop_set_phy_address(WRIOP1_DPMAC10, 0,
				      QSGMII2_PORT4_PHY_ADDR);

		break;
	default:
		printf("SerDes1 protocol 0x%x is not supported on LS1088ARDB\n",
		       srds_s1);
		break;
	}

	for (i = WRIOP1_DPMAC3; i <= WRIOP1_DPMAC10; i++) {
		interface = wriop_get_enet_if(i);
		switch (interface) {
		case PHY_INTERFACE_MODE_QSGMII:
			dev = miiphy_get_dev_by_name(DEFAULT_WRIOP_MDIO1_NAME);
			wriop_set_mdio(i, dev);
			break;
		default:
			break;
		}
	}

	dev = miiphy_get_dev_by_name(DEFAULT_WRIOP_MDIO2_NAME);
	wriop_set_mdio(WRIOP1_DPMAC2, dev);

	cpu_eth_init(bis);
#endif /* CONFIG_FMAN_ENET */
	printf("board_eth_init end\n");
	return pci_eth_init(bis);
}

#if defined(CONFIG_RESET_PHY_R)
void reset_phy(void)
{
	mc_env_boot();
}
#endif /* CONFIG_RESET_PHY_R */

int board_phy_config(struct phy_device *phydev) {
	unsigned short id1, id2;
	unsigned short val;
	id1 = phy_read(phydev, MDIO_DEVAD_NONE, 2);
	id2 = phy_read(phydev, MDIO_DEVAD_NONE, 3);
	if ((id1 == 0x7) && (id2 == 0x670)) {
		#if 0
		printf("TEN64 board_phy_config %d\n",phys_configured);
		#endif
		/* First, ensure LEDs are driven to rails (not tristate)
		 * This is in the extended page 0x0010
		 */
		phy_write(phydev, MDIO_DEVAD_NONE, 0x1F, 0x0010);
		phy_write(phydev, MDIO_DEVAD_NONE, 0x0E, 0x2000);
		/* Restore to page 0 */
		phy_write(phydev, MDIO_DEVAD_NONE, 0x1F, 0x0000);

		/* Disable blink on the left LEDs, and make the activity LEDs blink faster */
		phy_write(phydev, MDIO_DEVAD_NONE, 0x1E, 0xC03);

		/* RevA had reversed LED polarities for top and bottom ports, configure them differently */
		if (((phys_configured+1) % 4) == 0) {
			phy_write(phydev, MDIO_DEVAD_NONE, 0x1D, 0x4132);
			phy_write(phydev, MDIO_DEVAD_NONE, 0x1E, 0x5);
		} else if (((phys_configured+1) % 2) == 0) {
			phy_write(phydev, MDIO_DEVAD_NONE, 0x1D, 0x4312);
		} else {
			phy_write(phydev, MDIO_DEVAD_NONE, 0x1D, 0x3421);
		}
		phys_configured++;
	}
	if (phydev->drv->config)
		phydev->drv->config(phydev);

	return 0;
}
