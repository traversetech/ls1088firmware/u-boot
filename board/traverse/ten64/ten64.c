
// SPDX-License-Identifier: GPL-2.0+
/* Traverse Ten64 Board Support
 * Copyright 2017-2018 NXP
 * Copyright 2019 Traverse Technologies Australia
 */

#include <common.h>
#include <asm/arch/soc.h>
#include <fsl_ddr.h>
#include <fsl_sec.h>
#include <fsl_ifc.h>
#include <asm/arch-fsl-layerscape/soc.h>
#include <asm/arch/ppa.h>
#include <fsl-mc/fsl_mc.h>
#include <i2c.h>

#define MACADDRBITS(a,b) (uint8_t)((a >> b) & 0xFF)

DECLARE_GLOBAL_DATA_PTR;

void board_retimer_ds110df410_init(void);

/* FIXME: Only allow ft_board_setup to be run once
 * per boot. Otherwise the IOMMU and PCIe setup code
 * in pcie_layerscape_fixup will be run twice
 * and overwrite the existing setup */

uint8_t has_done_fixup = 0;

int checkboard(void) {
#ifdef CONFIG_TFABOOT
    enum boot_src src = get_boot_src();
#endif
    printf("Board: 1064-0201A, boot from ");

#ifdef CONFIG_TFABOOT
    if (src == BOOT_SOURCE_SD_MMC) {
        puts("SD card\n");
    }
    else if (src == BOOT_SOURCE_QSPI_NOR) {
        puts("QSPI\n");
    } else {
        printf("Unknown boot source %d\n",src);
    }
#else
#ifdef CONFIG_SD_BOOT
    puts("SD card\n");
#endif
#endif
    return 0;
}

#ifdef CONFIG_RESV_RAM
/* The DPAA2/MC and other hardware blocks
 * will reserve a portion of memory
 * (see ./arch/arm/cpu/armv8/fsl-layerscape/doc/README.lsch3
 * for more information)
 * So the user isn't wondering what ate their RAM, print out
 * the effective memory size on boot.
 * See ft_board_setup for how this is effected in the device tree.
 */
void print_usable_mem_size(void)
{
    int i;

    u64 base[CONFIG_NR_DRAM_BANKS];
    u64 orig_size[CONFIG_NR_DRAM_BANKS];
    u64 avail_size[CONFIG_NR_DRAM_BANKS];

    for (i = 0; i < CONFIG_NR_DRAM_BANKS; i++) {
        base[i] = gd->bd->bi_dram[i].start;
        orig_size[i] = gd->bd->bi_dram[i].size;
    }

    avail_size[0] = orig_size[0];
    avail_size[1] = orig_size[1];

    /* reduce size if reserved memory is within this bank */
    if (gd->arch.resv_ram >= base[0] &&
            gd->arch.resv_ram < base[0] + orig_size[0]) {
        avail_size[0] = gd->arch.resv_ram - base[0];
    } else if (gd->arch.resv_ram >= base[1] &&
               gd->arch.resv_ram < base[1] + orig_size[1]) {
        avail_size[1] = gd->arch.resv_ram - base[1];
    }

    print_size(avail_size[0] + avail_size[1],"");
    puts(" available for userspace\n");
}
#endif

void detail_board_ddr_info(void)
{
    puts("\nDDR    ");
    print_size(gd->bd->bi_dram[0].size + gd->bd->bi_dram[1].size, "");
    print_ddr_info(0);

    #ifdef CONFIG_RESV_RAM
    puts("\n\t");
    print_usable_mem_size();
    #endif
}

/* This overrides board_show_dram in cmd/mem.c - extended
 * to show the MC memory reservation
 */
void board_show_dram(phys_size_t size)
{
  puts("DRAM: ");
  print_size(size, ", ");

  #ifdef CONFIG_RESV_RAM
  print_usable_mem_size();
  #endif
}

int board_init(void)
{
    init_final_memctl_regs();

#ifdef CONFIG_FSL_CAAM
    sec_init();
#endif
#ifdef CONFIG_FSL_LS_PPA
    ppa_init();
#endif

    board_retimer_ds110df410_init();

    return 0;
}

int board_early_init_f(void)
{
    fsl_lsch3_early_init_f();
    return 0;
}

#if defined(CONFIG_ARCH_MISC_INIT)
int arch_misc_init(void)
{
    return 0;
}
#endif

#ifdef CONFIG_MISC_INIT_R
int misc_init_r(void)
{
    return 0;
}
#endif

#ifdef CONFIG_FSL_MC_ENET
void board_quiesce_devices(void)
{
    fsl_mc_ldpaa_exit(gd->bd);
}

void fdt_fixup_board_enet(void *fdt)
{
    int offset;

    offset = fdt_path_offset(fdt, "/fsl-mc");

    if (offset < 0)
        offset = fdt_path_offset(fdt, "/soc/fsl-mc");

    if (offset < 0) {
        printf("%s: ERROR: fsl-mc node not found in device tree (error %d)\n",
               __func__, offset);
        return;
    }

    if (get_mc_boot_status() == 0 &&
            (is_lazy_dpl_addr_valid() || get_dpl_apply_status() == 0))
        fdt_status_okay(fdt, offset);
    else
        fdt_status_fail(fdt, offset);
}
#endif

#ifdef CONFIG_OF_BOARD_SETUP
int ft_board_setup(void *blob, bd_t *bd)
{
    int i;
    u64 base[CONFIG_NR_DRAM_BANKS];
    u64 size[CONFIG_NR_DRAM_BANKS];

    printf("TEN64 ft_board_setup start, blob %p\n",blob);
    if (has_done_fixup == 1) {
        printf("TEN64 ft_board_setup already run, not doing anything\n");
        return 0;
    }
    ft_cpu_setup(blob, bd);

    /* fixup DT for the two GPP DDR banks */
    for (i = 0; i < CONFIG_NR_DRAM_BANKS; i++) {
        base[i] = gd->bd->bi_dram[i].start;
        size[i] = gd->bd->bi_dram[i].size;
    }

#ifdef CONFIG_RESV_RAM
    /* reduce size if reserved memory is within this bank */
    if (gd->arch.resv_ram >= base[0] &&
            gd->arch.resv_ram < base[0] + size[0])
        size[0] = gd->arch.resv_ram - base[0];
    else if (gd->arch.resv_ram >= base[1] &&
             gd->arch.resv_ram < base[1] + size[1])
        size[1] = gd->arch.resv_ram - base[1];
#endif

    fdt_fixup_memory_banks(blob, base, size, CONFIG_NR_DRAM_BANKS);

    fdt_fsl_mc_fixup_iommu_map_entry(blob);

    //fsl_fdt_fixup_flash(blob);

#ifdef CONFIG_FSL_MC_ENET
    fdt_fixup_board_enet(blob);
#endif

    printf("TEN64 ft_board_setup end\n");
    has_done_fixup = 1;
    return 0;
}
#else
#error "CONFIG_OF_BOARD_SETUP is required for this board"
#endif

/*
 * Adopted from the t102xqds board file
 */
void board_retimer_ds110df410_init(void)
{
    u8 reg;
    int ret;
    struct udevice *dev;
    puts("Retimer: ");

    /* Access to Control/Shared register */
    reg = 0x0;
    ret = i2c_get_chip_for_busnum(0, I2C_RETIMER_ADDR, 1, &dev);

    dm_i2c_write(dev, 0xff, &reg, 1);

    /* Read device revision and ID */
    dm_i2c_read(dev, 1, &reg, 1);
    if (reg == 0xF0) {
        printf("DS110DF410 found\n");
    } else {
        printf("Unknown retimer 0x%xn\n", reg);
    }

    /* Enable Broadcast */
    reg = 0x0c;
    dm_i2c_write(dev, 0xff, &reg, 1);

    /* Reset Channel Registers */
    dm_i2c_read(dev, 0, &reg, 1);
    reg |= 0x4;
    dm_i2c_write(dev, 0, &reg, 1);

    /* Set rate/subrate = 0 */
    reg = 0x6;
    dm_i2c_write(dev, 0x2F, &reg, 1);

#if 0
    /* Select active PFD MUX input as re-timed data (001) */
    dm_i2c_read(dev, 0x1e, &reg, 1);
    reg &= 0x3f;
    reg |= 0x20;
    dm_i2c_write(dev, 0x1e, &reg, 1);
#endif

    /* Set data rate as 10.3125 Gbps */
    reg = 0x0;
    dm_i2c_write(dev, 0x60, &reg, 1);
    reg = 0xb2;
    dm_i2c_write(dev, 0x61, &reg, 1);
    reg = 0x90;
    dm_i2c_write(dev, 0x62, &reg, 1);
    reg = 0xb3;
    dm_i2c_write(dev, 0x63, &reg, 1);
    reg = 0xff;
    dm_i2c_write(dev, 0x64, &reg, 1);

#if 0
    i2c_read(I2C_RETIMER_ADDR, 0x1F, 1, &reg, 1);
//		printf("Reg 0x1F value was=%X\n",reg);
    reg |= 0x80;
//		printf("Reg 0x1F value now=%X\n", reg);
    i2c_write(I2C_RETIMER_ADDR, 0x1F, 1, &reg, 1);
#endif

    /* Invert channel 2 (Lower SFP TX to CPU) due to the SFP being inverted */
    reg = 0x05;
    dm_i2c_write(dev, 0xFF, &reg, 1);
    dm_i2c_read(dev, 0x1F, &reg, 1);
    reg |= 0x80;
    dm_i2c_write(dev, 0x1F, &reg, 1);
    printf("\tChannel B invert applied\n");

#if 0
    /* Channel B needs a boost */
    reg = 0x05;
    i2c_write(I2C_RETIMER_ADDR, 0xFF, &reg, 1);
    reg = 0x82;

    i2c_write(I2C_RETIMER_ADDR, 0x2D, &reg, 1);
#endif

    //i2c_set_bus_num(0);

    puts("OK\n");
}

#ifdef CONFIG_TFABOOT
void *env_sf_get_env_addr(void)
{
    return (void *)(CONFIG_SYS_FSL_QSPI_BASE + CONFIG_ENV_OFFSET);
}
#endif

int mac_read_from_eeprom(void) {
    uint8_t macaddr_bytes[6];
    uint64_t macaddr;
    int ret;
    char ethaddr[18];
    char enetvar[10];
    int i,this_dpmac_num;
    struct udevice *dev;

    /** MAC addresses are allocated in order of the physical port numbers,
     * DPMAC7->10 is "eth0" through "eth3"
     * DPMAC3->6 is "eth4" through "eth7"
     * DPMAC1 and 2 are "eth8" and "eth9" respectively
     */
    int allocation_order[10] = {7, 8, 9, 10, 3, 4, 5, 6, 1, 2};

    puts("uC:    ");
    ret = i2c_get_chip_for_busnum(0, 0x7E, 1, &dev);
    if (ret) {
        puts("ERROR selecting uC\n");
        return -1;
    }
    //i2c_set_bus_num(3);
    ret = dm_i2c_read(dev, 0x20, (void *)&macaddr_bytes, 6);
    if (ret) {
        puts("ERROR reading from uC EEPROM\r\n");
        /* Ignore this error so the user can override */
        return 0;
    }
    /* We need to convert to a 64-bit integer to do increment the MAC on every interface */
    macaddr = (uint64_t)(((uint64_t)macaddr_bytes[0] << 40) & 0xFF0000000000) |
              (uint64_t)(((uint64_t)macaddr_bytes[1] << 32) & 0xFF00000000) |
              (uint64_t)((macaddr_bytes[2] << 24) & 0xFF000000) |
              (uint64_t)((macaddr_bytes[3] << 16) & 0xFF0000) |
              (uint64_t)((macaddr_bytes[4] << 8) & 0xFF00) |
              (uint64_t)((macaddr_bytes[5] & 0xFF));
    printf("MACADDR: %llx\n", macaddr);
    //macaddr = MACADDRBITS(macaddr,40) | MACADDRBITS(macaddr,32) | MACADDRBITS(macaddr,24) | MACADDRBITS(macaddr,16) | MACADDRBITS(macaddr,8) | MACADDRBITS(macaddr,0) << 40

    for(i=0; i<10; i++) {
        snprintf(ethaddr, 18, "%02X:%02X:%02X:%02X:%02X:%02X",
                 MACADDRBITS(macaddr,40),
                 MACADDRBITS(macaddr,32),
                 MACADDRBITS(macaddr,24),
                 MACADDRBITS(macaddr,16),
                 MACADDRBITS(macaddr,8),
                 MACADDRBITS(macaddr,0));

        this_dpmac_num = allocation_order[i];
        printf("DPMAC%d: %s\n", this_dpmac_num, ethaddr);
        snprintf(enetvar,10, (this_dpmac_num != 1) ? "eth%daddr" : "ethaddr", this_dpmac_num-1);
        macaddr++;
        if (!env_get(enetvar))
            env_set(enetvar,ethaddr);
    }
    //puts("OK\n");
    //i2c_set_bus_num(0);
    return 0;
}
int do_mac(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]) {
    char cmd;
    cmd = argv[1][0];

    if (cmd == 'r') {
        return mac_read_from_eeprom();
    }
    return 0;
}
