// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (C) 2019 Traverse Technologies
 *
 * Author: Mathew McBride <matt@traverse.com.au>
 * Based on paragon.c by Jeff Kletsky
 */

 #ifndef __UBOOT__
 #include <linux/device.h>
 #include <linux/kernel.h>
 #endif
 #include <linux/mtd/spinand.h>

#define SPINAND_MFR_ESMT 0xC8

#define F501DXG64_STATUS_ECC_BITMASK		(3 << 4)

#define F501DXG64_STATUS_ECC_NONE_DETECTED	(0 << 4)
#define F501DXG64_STATUS_ECC_1_7_CORRECTED	(1 << 4)
#define F501DXG64_STATUS_ECC_ERRORED		(2 << 4)
#define F501DXG64_STATUS_ECC_8_CORRECTED		(3 << 4)

static SPINAND_OP_VARIANTS(read_cache_variants,
                          SPINAND_PAGE_READ_FROM_CACHE_X4_OP(0, 1, NULL, 0),
                          SPINAND_PAGE_READ_FROM_CACHE_X2_OP(0, 1, NULL, 0),
                          SPINAND_PAGE_READ_FROM_CACHE_OP(true, 0, 1, NULL, 0),
                          SPINAND_PAGE_READ_FROM_CACHE_OP(false, 0, 1, NULL, 0));

static SPINAND_OP_VARIANTS(write_cache_variants,
                           SPINAND_PROG_LOAD_X4(true, 0, NULL, 0),
                           SPINAND_PROG_LOAD(true, 0, NULL, 0));

static SPINAND_OP_VARIANTS(update_cache_variants,
                           SPINAND_PROG_LOAD_X4(false, 0, NULL, 0),
                           SPINAND_PROG_LOAD(false, 0, NULL, 0));

static int f501dxg64_ooblayout_ecc(struct mtd_info *mtd, int section,
                                   struct mtd_oob_region *region)
{
    if (section > 3)
        return -ERANGE;

    region->offset = 6 + (15 * section); /* 4 BBM + 2 user bytes */
    region->length = 13;

    return 0;
}

static int f501dxg64_ecc_get_status(struct spinand_device *spinand,
				   u8 status)
{
	switch (status & F501DXG64_STATUS_ECC_BITMASK) {
	case F501DXG64_STATUS_ECC_NONE_DETECTED:
		return 0;

	case F501DXG64_STATUS_ECC_1_7_CORRECTED:
		return 7;	/* Return upper limit by convention */

	case F501DXG64_STATUS_ECC_8_CORRECTED:
		return 8;

	case F501DXG64_STATUS_ECC_ERRORED:
		return -EBADMSG;

	default:
		break;
	}

	return -EINVAL;
}


static int f501dxg64_ooblayout_free(struct mtd_info *mtd, int section,
                                    struct mtd_oob_region *region)
{
    if (section > 4)
        return -ERANGE;

    if (section == 4) {
        region->offset = 64;
        region->length = 64;
    } else {
        region->offset = 4 + (15 * section);
        region->length = 2;
    }

    return 0;
}


static const struct mtd_ooblayout_ops f501dxg64_ooblayout = {
    .ecc = f501dxg64_ooblayout_ecc,
    .free = f501dxg64_ooblayout_free,
};

static const struct spinand_info esmt_spinand_table[] = {
    SPINAND_INFO("F50D1G41LB", 0x11,
                 NAND_MEMORG(1, 2048, 64, 64, 1024, 1, 1, 1),
                 NAND_ECCREQ(8, 512),
                 SPINAND_INFO_OP_VARIANTS(&read_cache_variants,
                                          &write_cache_variants,
                                          &update_cache_variants),
                 0,
                 SPINAND_ECCINFO(&f501dxg64_ooblayout,
                                 f501dxg64_ecc_get_status)),
};

static int esmt_spinand_detect(struct spinand_device *spinand)
{
    u8 *id = spinand->id.data;
    int ret;
    printf("esmt_spinand_detect have id %X %X %X\n", id[0], id[1], id[2]);
    /* Read ID returns [0][MID][DID] */

    if (id[1] != SPINAND_MFR_ESMT)
        return 0;

    ret = spinand_match_and_init(spinand, esmt_spinand_table,
                                 ARRAY_SIZE(esmt_spinand_table),
                                 id[2]);
    if (ret)
        return ret;
    printf("esmt_spinand_detect returned with match\n");
    return 1;
}

static const struct spinand_manufacturer_ops esmt_spinand_manuf_ops = {
    .detect = esmt_spinand_detect,
};

const struct spinand_manufacturer esmt_spinand_manufacturer = {
    .id = SPINAND_MFR_ESMT,
    .name = "ESMT",
    .ops = &esmt_spinand_manuf_ops,
};
